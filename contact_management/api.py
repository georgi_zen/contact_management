from flask import Blueprint, jsonify, request
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from contact_management.models import Contact
from contact_management.extensions import db

api = Blueprint('api', __name__)

def success_response(data):
    return jsonify({'status': 'OK', 'data': data})

def error_response(message=None):
    return jsonify({'status': 'ERROR', 'message': message})

@api.route('/contacts', methods=['GET'])
def get_contacts():

    contacts = [contact.to_dict() for contact in Contact.query.order_by(Contact.id.desc()).all()]

    return success_response(contacts)

@api.route('/contacts', methods=['POST'])
def add_contact():

    data = request.get_json(force=True)

    try:
        contact = Contact()
        contact.from_dict(data)

        db.session.add(contact)
        db.session.commit()

        return success_response(contact.to_dict())

    except IntegrityError:

        return error_response('Contact with username ' + data['username'] + ' already exists!')

    except AssertionError as e:

        return error_response(str(e))

@api.route('/contacts/<username>', methods=['GET'])
def get_contact(username):

    try:

        contact = Contact.query.filter(Contact.username==username).one()
        return success_response(contact.to_dict())

    except NoResultFound:

        return error_response('Contact with username ' + username + ' not found!')

@api.route('/contacts/<username>', methods=['DELETE'])
def delete_contact(username):

    try:

        contact = Contact.query.filter(Contact.username==username).one()

        db.session.delete(contact)
        db.session.commit()

        return success_response('Successfully deleted contact with username ' + contact.username)

    except NoResultFound:

        return error_response('Contact with username ' + username + ' not found!')

@api.route('/contacts/<username>', methods=['PUT'])
def update_contact(username):

    data = request.get_json(force=True)

    try:

        contact = Contact.query.filter(Contact.username==username).one()

        contact.from_dict(data)

        db.session.commit()

        return success_response(contact.to_dict())

    except IntegrityError:

        return error_response('Contact with username ' + data['username'] + ' already exists!')

    except AssertionError as e:

        return error_response(str(e))


    except NoResultFound:

        return error_response('Contact with username ' + username + ' not found!')

