from flask_sqlalchemy import SQLAlchemy

from sqlalchemy.engine import Engine
from sqlalchemy import event

from celery import Celery

from config import CELERY_BROKER_URL

db = SQLAlchemy()
celery = Celery('contact_management', broker=CELERY_BROKER_URL, include=['contact_management.tasks'])


#sqlite locks the database on write so we will use WAL: https://sqlite.org/wal.html
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA journal_mode=WAL")
    cursor.close()
