from flask import Flask

from contact_management.extensions import db, celery
from contact_management.api import api

def create_app():

    app = Flask(__name__)
    app.config.from_object('config')

    register_extensions(app)

    register_blueprints(app)

    db.drop_all(app=app)
    db.create_all(app=app)

    # @app.teardown_request
    # def remove_db_session(exception):
    #     db.session.remove()

    return app


def register_extensions(app):

    db.init_app(app)
    # celery.conf.update(app.config)
    celery.conf.result_backend = app.config['CELERY_RESULT_BACKEND']

    return app

def register_blueprints(app):

    app.register_blueprint(api, url_prefix='/api')

    return app

