import re

from datetime import datetime
from sqlalchemy.orm import validates
from sqlalchemy import event

from flask import current_app

from contact_management.extensions import db, celery


class IdMixin(object):

    id = db.Column(db.Integer, primary_key=True)

class TimestampMixin(object):

    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)

class DictMixin(object):

    def to_dict(self):

        data = {}
        for key in self.__table__.columns.keys():
            data[key] = eval('self.' + key)

        return data

    def from_dict(self, data):

        for field in self.editable_fields:
            if field in data:
                setattr(self, field, data[field])
            else:
                setattr(self, field, None)

class Contact(db.Model, IdMixin, TimestampMixin, DictMixin):

    __tablename__ = 'contacts'

    editable_fields = ['username', 'first_name', 'last_name']

    username = db.Column(db.String(200), unique=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))

    emails = db.relationship("Email", lazy="joined", cascade="all, delete-orphan")

    @validates(*editable_fields)
    def validate_field(seld, key, value):
        assert isinstance(value, str) and len(value) > 0, 'Empty value for ' + key
        return value

    def from_dict(self, data):

        super(Contact, self).from_dict(data)

        if 'emails' not in data:
            self.emails.clear()
        else:
            emails_to_add = []
            for email in data['emails']:
                email_obj = Email()
                email_obj.email = email
                if email_obj not in self.emails:
                    emails_to_add.append(email_obj)

            for email in self.emails:
                if email not in data['emails']:
                    self.emails.remove(email)

            self.emails.extend(emails_to_add)


    def to_dict(self):
        data = super(Contact, self).to_dict()
        data['emails'] = []
        for email in self.emails:
            data['emails'].append(email.email)
        return data

class Email(db.Model, IdMixin):

    __tablename__ = 'emails'

    email = db.Column(db.String(200), unique=True)
    contact_id = db.Column(db.Integer, db.ForeignKey('contacts.id'))

    @validates('email')
    def validate_field(seld, key, value):
        assert isinstance(value, str) and len(value) > 0, 'Empty value for ' + key
        assert re.match(r'[^@]+@[^@]+\.[^@]+', value), 'Email address is not valid'
        return value

    def __eq__(self, obj):
        return (hasattr(obj, 'email') and self.email == obj.email) or self.email == obj

def delete_older_contact_delay(mapper, connection, target):

    celery.send_task('contact_management.tasks.delete_older_contact', (target.id,), countdown=60, retries=5)


event.listen(Contact, 'after_insert', delete_older_contact_delay)
