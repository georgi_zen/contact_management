import string
import random

from contact_management.extensions import db, celery
from contact_management.models import Contact

celery.conf.beat_schedule = {
    'random_contact_every_15s':
        {
            'task': 'contact_management.tasks.add_random_contact',
            'schedule': 15
        }
    }


def generate_random_string(max_length):
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(random.randint(1, max_length)))

@celery.task
def add_random_contact():

    c = Contact()

    data = {
            'username': generate_random_string(20),
            'first_name': generate_random_string(20),
            'last_name': generate_random_string(20),
            'emails': []
    }
    for i in range(random.randint(1, 5)):
        data['emails'].append(generate_random_string(5) + '@' + generate_random_string(5) + '.' + generate_random_string(3))

    c.from_dict(data)

    db.session.add(c)
    db.session.commit()

    return c.username

@celery.task
def delete_older_contact(contact_id):

    c = Contact.query.filter(Contact.id==contact_id).one()

    db.session.delete(c)
    db.session.commit()

    return True
