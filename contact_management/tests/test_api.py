import pytest
from json import loads

from contact_management.factories.application import create_app
from contact_management.factories.celery import create_celery
from contact_management.extensions import db
from contact_management.tasks import add_random_contact, delete_older_contact

app = create_app()
celery = create_celery(app)
celery.conf.update(task_always_eager=True)

@pytest.fixture
def client():

    with app.test_client() as client:

        yield client

def find_contact(client, username):

    request = client.get('/api/contacts/' + username)

    response = request.get_json()

    return response

def add_contact(client, data):

    request = client.post('/api/contacts', json=data)

    response = request.get_json()

    return response

def update_contact(client, username, data):

    request = client.put('/api/contacts/' + username, json=data)

    response = request.get_json()

    return response


def test_add_contact(client):

    data = {'username': 'test_user_no_emails', 'first_name': 'test', 'last_name': 'user'}

    assert add_contact(client, data)['status'] == 'OK'
    assert find_contact(client, 'test_user_no_emails')['status'] == 'OK'

    data = {'username': 'test_user', 'first_name': 'test', 'last_name': 'user', 'emails': ['asd@asd.asd', 'qwe@qweqwe.qwe']}

    assert add_contact(client, data)['status'] == 'OK'
    find_contact_query = find_contact(client, 'test_user')
    assert find_contact_query['status'] == 'OK'
    assert 'asd@asd.asd' in find_contact_query['data']['emails']
    assert 'qwe@qweqwe.qwe' in find_contact_query['data']['emails']

def test_add_contact_fail(client):

    #usernames are unique and all fields are required

    data = {'username': 'test_user', 'first_name': 'test', 'last_name': 'user'}
    assert add_contact(client, data)['status'] == 'ERROR'
    data = {'username': 'test_user', 'first_name': 'test'}
    assert add_contact(client, data)['status'] == 'ERROR'
    data = {'username': 'test_user', 'last_name': 'user'}
    assert add_contact(client, data)['status'] == 'ERROR'
    data = {'first_name': 'test', 'last_name': 'user'}
    assert add_contact(client, data)['status'] == 'ERROR'

    data = {'username': 'test_user_wrong_emails', 'first_name': 'test', 'last_name': 'user', 'emails': ['wejrlkwejrl', '@qwel@qwewqe@qwqw']}
    assert add_contact(client, data)['status'] == 'ERROR'


def test_find_contact(client):

    query = find_contact(client, 'test_user')
    assert query['status'] == 'OK'
    assert 'asd@asd.asd' in query['data']['emails']


def test_update_contact(client):

    data = {'username': 'test_user_updated', 'first_name': 'test', 'last_name': 'user'}

    assert update_contact(client, 'test_user', data)['status'] == 'OK'

    assert find_contact(client, 'test_user_updated')['status'] == 'OK'
    assert find_contact(client, 'test_user')['status'] == 'ERROR'
    query = find_contact(client, 'test_user_updated')
    assert len(query['data']['emails']) == 0

    data = {'username': 'test_user_updated', 'first_name': 'test', 'last_name': 'user', 'emails': ['asd@asd.asd', 'new@email.one']}
    update_query = update_contact(client, 'test_user_updated', data)
    assert update_query['status'] == 'OK'
    assert 'new@email.one' in update_query['data']['emails']

    data = {'username': 'test_user_updated', 'first_name': 'test', 'last_name': 'user', 'emails': ['asd@asd.asd']}
    update_query = update_contact(client, 'test_user_updated', data)
    assert update_query['status'] == 'OK'
    assert 'new@email.one' not in update_query['data']['emails']

def test_update_contact_fail(client):

    data = {'username': 'test_user_updated', 'first_name': 'test', 'last_name': 'user update'}

    assert update_contact(client, 'test_user', data)['status'] == 'ERROR'

    data['username'] = 'test_user'

    assert add_contact(client, data)['status'] == 'OK'
    assert update_contact(client, 'test_user_updated', data)['status'] == 'ERROR' #same username already exists

    assert update_contact(client, 'test_user', data)['status'] == 'OK'
    data = {'username': 'test_user', 'first_name': 'test'}
    assert update_contact(client, 'test_user', data)['status'] == 'ERROR'
    data = {'username': 'test_user', 'last_name': 'user'}
    assert update_contact(client, 'test_user', data)['status'] == 'ERROR'
    data = {'first_name': 'test', 'last_name': 'user'}
    assert update_contact(client, 'test_user', data)['status'] == 'ERROR'



def test_get_contacts(client):

    request = client.get('/api/contacts')

    response = request.get_json()

    assert response['status'] == 'OK'

    assert b'test_user_updated' in request.data

def test_delete_contact(client):

    request = client.delete('/api/contacts/test_user_updated')

    response = request.get_json()

    assert response['status'] == 'OK'

    request = client.get('/api/contacts')

    response = request.get_json()

    assert response['status'] == 'OK'
    assert b'test_user_updated' not in request.data

def test_delete_missing_client(client):

    request = client.delete('/api/contacts/test_user_missing')

    response = request.get_json()

    assert response['status'] == 'ERROR'

def test_missing_contact(client):

    assert find_contact(client, 'test_user_missing')['status'] == 'ERROR'

def test_add_random_contact(client):

    contact_username = add_random_contact.delay().get()

    assert find_contact(client, contact_username)['status'] == 'OK'

def test_delete_older_contact(client):

    contact_username = add_random_contact.delay().get()

    query = find_contact(client, contact_username)

    assert query['status'] == 'OK'

    delete_older_contact.delay(query['data']['id']).get()

    query = find_contact(client, contact_username)

    assert query['status'] == 'ERROR'
