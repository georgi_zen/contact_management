from contact_management.factories.application import create_app
from contact_management.factories.celery import create_celery

celery = create_celery(create_app())

