from contact_management.factories.application import create_app

app = create_app()

app.run(debug=True)
