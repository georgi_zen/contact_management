#!/bin/bash

source venv/bin/activate

celery worker -A contact_management.worker -B &
python run.py
